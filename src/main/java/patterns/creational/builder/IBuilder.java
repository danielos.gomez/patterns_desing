package patterns.creational.builder;

public interface IBuilder<T> {
    T build();
}
