package patterns.structure.facade;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TestFacade {

    public static void main(String[] args) {

        Facade facade = new Facade();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String dateIni = "19/08/2022";
        String dateFin = "21/08/2022";
        facade.searchVacations(
                "Cucuta",
                "Medellin",
                LocalDate.parse(dateIni, formatter),
                LocalDate.parse(dateFin, formatter)
        );
    }
}
