package patterns.structure.decorator;

public class SavingsAccount implements IAccount{


    @Override
    public void createAccount(Account account) {
        System.out.println("Creating Savings Account...");
        System.out.println("Account Number is : " + account.number);
    }
}
