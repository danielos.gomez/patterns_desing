package patterns.creational.factory;

public class ConexionPostgresql implements IConexion {
    @Override
    public String ConexionOn() {
        return "Conexion On - Postgresql";
    }

    @Override
    public String ConexionOff() {
        return "Conexion Off - Postgresql";
    }
}
