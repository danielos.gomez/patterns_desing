package patterns.creational.abstract_factory.connect_api;

public class ConexionApiCompras implements IConexionApi{
    @Override
    public Integer getTotal() {
        return 0;
    }

    @Override
    public Integer getLastValue() {
        return 0;
    }
}
