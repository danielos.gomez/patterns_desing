package patterns.structure.decorator.decorator;

import patterns.structure.decorator.Account;
import patterns.structure.decorator.IAccount;

public class BlindDecorator extends AccountDecorator{

    public BlindDecorator(IAccount decoratedAccount) {
        super(decoratedAccount);
    }

@Override
public void createAccount(Account account) {
        super.decoratedAccount.createAccount(account);
        blindAccount(account);
    }

public void blindAccount(Account account){
        System.out.println("Blinding Account , Number : "+account.getNumber());
        System.out.println("Account Blinded");
    }
}
