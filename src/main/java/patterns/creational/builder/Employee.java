package patterns.creational.builder;

public class Employee {
    String firstname;
    String lastname;
    String document;
    Address address;

    public Employee() {
    }

    public Employee(String firstname, String lastname, String document, Address address) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.document = document;
        this.address = address;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", document='" + document + '\'' +
                ", address=" + address.toString()+
                '}';
    }
}
