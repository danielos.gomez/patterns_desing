package patterns.creational.factory;

public class ConexionMysql implements IConexion {
    @Override
    public String ConexionOn() {
        return "Conexion On - Mysql";
    }

    @Override
    public String ConexionOff() {
        return "Conexion Of - Mysql";
    }
}
