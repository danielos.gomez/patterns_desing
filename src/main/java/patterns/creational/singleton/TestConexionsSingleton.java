package patterns.creational.singleton;

public class TestConexionsSingleton {

    public static void main(String[] args) {
        Conexion conn =  Conexion.getInstance();

        /* This can not be done
         Conexion conn2 = new Conexion();
        */
        
        Boolean valid = conn instanceof Conexion;
        System.out.println(valid);
    }
}
