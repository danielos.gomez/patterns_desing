package patterns.creational.factory;

public class TestConexionFactory {
    public static void main(String[] args) {
        Factory factory = new Factory();
        IConexion conex = factory.factoryConexion("oracle");

        System.out.println(conex.ConexionOn());
        System.out.println(conex.ConexionOff());
    }
}
