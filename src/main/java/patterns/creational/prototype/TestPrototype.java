package patterns.creational.prototype;

public class TestPrototype {
    public static void main(String[] args) {
        Car car = new Car("Aveo" , "Chevrolet");
        Car car2 = (Car) car.cloneObject();

        System.out.println("Original Car : "+ car.toString());
        System.out.println("Cloned Car   : "+ car2.toString());

    }
}
