package patterns.creational.factory;

public class ConexionOracle implements IConexion {
    @Override
    public String ConexionOn() {
        return "Conexion On - Oracle";
    }

    @Override
    public String ConexionOff() {
        return "Conexion Off - Oracle";
    }
}
