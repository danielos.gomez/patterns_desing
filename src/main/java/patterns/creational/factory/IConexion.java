package patterns.creational.factory;

public interface IConexion {
    String ConexionOn();
    String ConexionOff();

}
