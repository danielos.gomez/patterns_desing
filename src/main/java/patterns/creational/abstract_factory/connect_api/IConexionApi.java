package patterns.creational.abstract_factory.connect_api;

public interface IConexionApi {

    Integer getTotal();
    Integer getLastValue();

}
