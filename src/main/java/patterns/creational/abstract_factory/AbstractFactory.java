package patterns.creational.abstract_factory;

import patterns.creational.abstract_factory.conect_bd.FactoryBd;
import patterns.creational.abstract_factory.connect_api.FactoryApi;

public class AbstractFactory {

    public IAbstractFactory getFactory (String name){
        if(name.equalsIgnoreCase("bd")){
            return new FactoryBd();
        }
        return new FactoryApi();
    }
}
