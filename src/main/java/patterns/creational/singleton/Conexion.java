package patterns.creational.singleton;

public class Conexion {

    private static Conexion conexion;

    private Conexion(){

    }

    static Conexion getInstance(){
        if(conexion==null){
            conexion = new Conexion();
        }
        return conexion;
    }

    public void connect(){
        System.out.println("Connected");
    }





}


