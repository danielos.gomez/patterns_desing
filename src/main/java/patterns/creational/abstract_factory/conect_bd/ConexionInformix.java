package patterns.creational.abstract_factory.conect_bd;

public class ConexionInformix implements IConexionBd {
    @Override
    public String ConexionOn() {
        return "Conexion On - Informix";
    }

    @Override
    public String ConexionOff() {
        return "Conexion Off - Informix";
    }
}
