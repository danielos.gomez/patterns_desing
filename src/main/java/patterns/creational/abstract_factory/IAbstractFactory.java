package patterns.creational.abstract_factory;

import patterns.creational.abstract_factory.conect_bd.IConexionBd;
import patterns.creational.abstract_factory.connect_api.IConexionApi;

public interface IAbstractFactory {
    IConexionApi getConexionApi(String name);
    IConexionBd getConexionBd (String name);
}
