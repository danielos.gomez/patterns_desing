package patterns.creational.builder;

public class EmployeeBuilder implements IBuilder<Employee>{

    private String firstname;
    private String lastname;
    private String document;
    private Address address;

    public EmployeeBuilder setFirstName(String firstname){
        this.firstname = firstname;
        return this;
    }

    public EmployeeBuilder setLastName(String lastname){
        this.lastname = lastname;
        return this;
    }

    public EmployeeBuilder setDocument(String document){
        this.document = document;
        return this;
    }

    public EmployeeBuilder setAddressWithoutCity(String avenue, String street, String number){
        this.address = new Address(avenue,street,number);
        return this;
    }

    public EmployeeBuilder setAddressWithCity(String avenue, String street, String number, String country,
                                              String state, String nameCity){
        this.address = new Address(avenue,street,number,new City(country,state,nameCity));
        return this;
    }

    @Override
    public Employee build() {
        return new Employee(firstname,lastname,document,address);
    }


}
