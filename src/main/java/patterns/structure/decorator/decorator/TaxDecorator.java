package patterns.structure.decorator.decorator;

import patterns.structure.decorator.Account;
import patterns.structure.decorator.IAccount;

public class TaxDecorator extends AccountDecorator{

    public TaxDecorator(IAccount decoratedAccount) {
        super(decoratedAccount);
    }

    @Override
    public void createAccount(Account account) {
        super.decoratedAccount.createAccount(account);
        taxAccount(account);

    }

    public void taxAccount(Account account){
        System.out.println("Taxing Account , Number : "+account.getNumber());
        System.out.println("Account Taxed");
    }
}
