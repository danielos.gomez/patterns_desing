package patterns.structure.decorator;

public class Account {

    Long Cuentaid;
    String number;
    String owner;

    public Account(Long cuentaid, String number, String owner) {
        Cuentaid = cuentaid;
        this.number = number;
        this.owner = owner;
    }

    public Long getCuentaid() {
        return Cuentaid;
    }

    public void setCuentaid(Long cuentaid) {
        Cuentaid = cuentaid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
