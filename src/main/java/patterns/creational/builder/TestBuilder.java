package patterns.creational.builder;

public class TestBuilder {
    public static void main(String[] args) {
        Employee employee = new EmployeeBuilder()
                .setFirstName("Daniel")
                .setLastName("Gomez")
                .setDocument("1090475088")
                .setAddressWithCity(
                        "20",
                        "27",
                        "101",
                        "Colombia",
                        "Norte de Santander",
                        "Cucuta")
                .build();

        System.out.println(employee.toString());
    }

}
