package patterns.creational.prototype;

public class Car implements ICloneable{

    String model, maker;

    public Car(String model , String maker) {
        this.model = model;
        this.maker = maker;
    }

    @Override
    public ICloneable cloneObject() {
        Car car2 = null;
        try {
            car2 = (Car) clone();
        } catch (Exception e){
            e.printStackTrace();
        }

        return car2;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", maker='" + maker + '\'' +
                '}';
    }


}
