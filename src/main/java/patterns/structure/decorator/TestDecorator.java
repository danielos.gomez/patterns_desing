package patterns.structure.decorator;

import patterns.structure.decorator.decorator.BlindDecorator;
import patterns.structure.decorator.decorator.TaxDecorator;

public class TestDecorator {
    public static void main(String[] args) {

       Account testAccount = new Account(1L,"12345","Daniel Gomez");

       // Only savings testAccount
       IAccount account1 = new CurrentAccount();
       account1.createAccount(testAccount);

        System.out.println("____________________________");

       //Saving Account + Blind
       IAccount blindDecorator = new BlindDecorator(account1);
       blindDecorator.createAccount(testAccount);

        System.out.println("____________________________");

       //Saving Account + Tax
        IAccount taxDecorator = new TaxDecorator(account1);
        taxDecorator.createAccount(testAccount);

        System.out.println("____________________________");

        //Saving Account + Blind + Tax
        IAccount taxBlindDecorator = new TaxDecorator(new BlindDecorator(account1));
        taxBlindDecorator.createAccount(testAccount);


    }
}
