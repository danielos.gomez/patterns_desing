package patterns.creational.factory;

public class ConexionGeneric implements IConexion{
    @Override
    public String ConexionOn() {
        return "ConexionOn - Generic";
    }

    @Override
    public String ConexionOff() {
        return "ConexionOff - Generic";
    }
}
