package patterns.creational.prototype;

public interface ICloneable extends Cloneable {

    ICloneable cloneObject();
}
