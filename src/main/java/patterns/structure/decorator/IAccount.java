package patterns.structure.decorator;

public interface IAccount {

    void createAccount(Account account);

}
