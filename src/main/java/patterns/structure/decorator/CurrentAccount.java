package patterns.structure.decorator;

public class CurrentAccount implements IAccount{


    @Override
    public void createAccount(Account account) {
        System.out.println("Creating Current Account...");
        System.out.println("Account Number is : " + account.number);
    }
}
