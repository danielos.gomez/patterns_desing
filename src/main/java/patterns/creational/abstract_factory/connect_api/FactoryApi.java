package patterns.creational.abstract_factory.connect_api;

import patterns.creational.abstract_factory.IAbstractFactory;
import patterns.creational.abstract_factory.conect_bd.IConexionBd;

public class FactoryApi implements IAbstractFactory {


    @Override
    public IConexionApi getConexionApi(String name) {
        if(name.equalsIgnoreCase("ventas")){
            return new ConexionApiVentas();
        } else {
            return new ConexionApiCompras();
        }
    }

    @Override
    public IConexionBd getConexionBd(String name) {
        return null;
    }
}
