package patterns.creational.builder;

public class Address {
    String avenue;
    String street;
    String number;
    City city;


    public Address(String avenue, String street, String number, City city) {
        this.avenue = avenue;
        this.street = street;
        this.number = number;
        this.city = city;
    }

    public Address(String avenue, String street, String number) {
        this.avenue = avenue;
        this.street = street;
        this.number = number;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address{" +
                "avenue='" + avenue + '\'' +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", city=" + city.toString() +
                '}';
    }
}
