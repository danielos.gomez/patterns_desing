package patterns.creational.abstract_factory.connect_api;

public class ConexionApiVentas implements IConexionApi{
    @Override
    public Integer getTotal() {
        return 1;
    }

    @Override
    public Integer getLastValue() {
        return 1;
    }
}
