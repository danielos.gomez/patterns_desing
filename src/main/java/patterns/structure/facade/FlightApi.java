package patterns.structure.facade;

import java.time.LocalDate;

public class FlightApi {

    public void PostFlight(LocalDate dateIni , LocalDate dateFin , String target , String source){
        System.out.println("Searching Flights to "+target+" ...");
        System.out.println("Flight 3245 - from "+source+" to "+target+" - " + "15:30");
        System.out.println("Flight 3146 - from "+source+" to "+target+" - " + "06:30");
    }
}
