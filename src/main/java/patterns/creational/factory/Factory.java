package patterns.creational.factory;

public class Factory {

    public IConexion factoryConexion (String name){
        if(name=="mysql") {
            return new ConexionMysql();
        }
        else if(name == "oracle"){
            return new ConexionOracle();
        }
        else if(name == "postgresql"){
            return new ConexionPostgresql();
        }
        else {
            return new ConexionGeneric();
        }
    }
}
