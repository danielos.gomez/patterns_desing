package patterns.creational.abstract_factory.conect_bd;

import patterns.creational.abstract_factory.IAbstractFactory;
import patterns.creational.abstract_factory.connect_api.IConexionApi;

public class FactoryBd implements IAbstractFactory {

    @Override
    public IConexionApi getConexionApi(String name) {
        return null;
    }

    @Override
    public IConexionBd getConexionBd(String name) {
        if(name.equalsIgnoreCase("firebird")){
            return new ConexionFirebird();
        } else {
            return new ConexionInformix();
        }
    }
}
