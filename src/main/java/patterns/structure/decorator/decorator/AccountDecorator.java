package patterns.structure.decorator.decorator;

import patterns.structure.decorator.Account;
import patterns.structure.decorator.IAccount;

public abstract class AccountDecorator implements IAccount{
    protected IAccount decoratedAccount;

    public AccountDecorator(IAccount decoratedAccount) {
        this.decoratedAccount = decoratedAccount;
    }

    @Override
    public void createAccount(Account account) {
        this.decoratedAccount.createAccount(account);
    }
}
