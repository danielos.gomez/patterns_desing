package patterns.structure.facade;

import java.time.LocalDate;

public class Facade {

    private FlightApi flights;
    private ReservationApi reservations;

    public Facade(){
        flights = new FlightApi();
        reservations = new ReservationApi();
    }

    public void searchVacations(String source, String target, LocalDate dateIni, LocalDate dateFin){
        flights.PostFlight(dateIni,dateFin,target,source);
        reservations.PostReservation(dateIni,dateFin,target);
    }
}
