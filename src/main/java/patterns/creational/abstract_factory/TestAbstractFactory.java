package patterns.creational.abstract_factory;

import patterns.creational.abstract_factory.conect_bd.IConexionBd;
import patterns.creational.abstract_factory.connect_api.IConexionApi;

public class TestAbstractFactory {
    public static void main(String[] args) {
        AbstractFactory abstractFactory = new AbstractFactory();

        /* BD */
        IAbstractFactory interfazBd = abstractFactory.getFactory("bd");
        IConexionBd value = interfazBd.getConexionBd("firebird");
        System.out.println(value.ConexionOn());
        System.out.println(value.ConexionOff());

        /* Api */
        IAbstractFactory interfazApi = abstractFactory.getFactory("api");
        IConexionApi value2 = interfazApi.getConexionApi("ventas");
        System.out.println(value2.getTotal());




    }
}
