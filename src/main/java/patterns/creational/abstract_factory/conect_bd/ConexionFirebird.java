package patterns.creational.abstract_factory.conect_bd;

public class ConexionFirebird implements IConexionBd {
    @Override
    public String ConexionOn() {
        return "Conexion On - Firebird";
    }

    @Override
    public String ConexionOff() {
        return "Conexion Off - Firebird";
    }
}
