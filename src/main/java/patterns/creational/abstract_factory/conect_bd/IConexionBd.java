package patterns.creational.abstract_factory.conect_bd;

public interface IConexionBd {
    String ConexionOn();
    String ConexionOff();

}
